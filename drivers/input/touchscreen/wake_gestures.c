/*
 * drivers/input/touchscreen/wake_gestures.c
 *
 * Copyright (c) 2013, Dennis Rassmann <showp1984@gmail.com>
 * Copyright (c) 2013-15 Aaron Segaert <asegaert@gmail.com>
 * Copyright (c) 2015, Ángel Palomar <placiano80@gmail.com>
 * Copyright (c) 2015, Pavel Nikolov <pafcholini@gmail.com>
 * Copyright (c) 2016, Dead-neM  <deadnem@gmail.com>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/err.h>
#include <linux/wake_gestures.h>
#include <linux/slab.h>
#include <linux/workqueue.h>
#include <linux/input.h>
#include <linux/hrtimer.h>
#include <asm-generic/cputime.h>
#include <linux/wakelock.h>
#include <linux/powersuspend.h>

/* Tuneables */
#define DEBUG_DEFAULT		0
#define T2W_DEFAULT		0
#define DT2W_DEFAULT		0
#define DT2S_DEFAULT		0
#define S2W_DEFAULT		0
#define S2S_DEFAULT		0
#define CAMERA_DEFAULT		0
#define SUSPEND_DEFAULT		0
#define TE_DEFAULT		0
#define TWT_DEFAULT		2000 * 1000L
#define SWT_DEFAULT		10000 * 1000L
#define WG_PWRKEY_DUR           100

/* Screen Size */
#define SWEEP_Y_MAX             2559
#define SWEEP_X_MAX             1439
/*Edge limit*/
#define SWEEP_EDGE		130
#define SWEEP_Y_LIMIT           SWEEP_Y_MAX-SWEEP_EDGE
#define SWEEP_X_LIMIT           SWEEP_X_MAX-SWEEP_EDGE
#define DT2S_Y_LIMIT		100
#define DT2S_X_LIMIT		1440
#define HS_EDGE_Y		1280
#define HS_EDGE_X		160
#define DT2W_HS_Y            	SWEEP_Y_MAX-HS_EDGE_Y
#define DT2W_HS_X          	SWEEP_X_MAX-HS_EDGE_X
/*Double tap square size*/
#define DT2W_FEATHER_X		200
#define DT2W_FEATHER_Y		200
#define DT2S_FEATHER		150
/*Sweep start & end*/
#define SWEEP_X_B1              532
#define SWEEP_X_B2              960
#define SWEEP_Y_START		1066
#define SWEEP_X_START		720
#define SWEEP_X_FINAL           360
#define SWEEP_Y_NEXT            180
/*Double tap time*/
#define DT2W_TIME 		200
#define DT2S_TIME		200

/* Wake Gestures */
#define WAKE_TIMEOUT		2000
#define SWEEP_TIMEOUT		100
#define TRIGGER_TIMEOUT		160
#define SWEEP_RIGHT		0x01
#define SWEEP_LEFT		0x02
#define SWEEP_UP		0x04
#define SWEEP_DOWN		0x08
#define VIB_STRENGTH 		44

#define LOGTAG			"WG"

/* Resources */
int debug = DEBUG_DEFAULT;
int t2w_switch = T2W_DEFAULT;
int dt2w_switch;
int dt2s_switch = DT2S_DEFAULT;
int s2w_switch = S2W_DEFAULT;
int camera_switch = CAMERA_DEFAULT;
int s2s_switch = S2S_DEFAULT;
int dt2s_x = DT2S_X_LIMIT;
int dt2s_y = DT2S_Y_LIMIT;
int dt2w_time = DT2W_TIME;
int dt2s_time = DT2S_TIME;
int dt2w_feather_x = DT2W_FEATHER_X;
int dt2w_feather_y = DT2W_FEATHER_Y;
int vib_strength = VIB_STRENGTH;
int gesture_suspend = SUSPEND_DEFAULT;
int timer_enabled = TE_DEFAULT;
static u64 tap_wake_time = TWT_DEFAULT;
static u64 sweep_wake_time = SWT_DEFAULT;
/*Static int*/
static bool camera = false;
static int touch_x = 0, touch_y = 0;
static bool touch_x_called = false, touch_y_called = false;
static bool exec_countx = true, exec_county = true, exec_count = true;
static bool barrierx[2] = {false, false}, barriery[2] = {false, false};
static int firstx = 0, firsty = 0;
static unsigned long firstx_time = 0, firsty_time = 0;
static unsigned long pwrtrigger_time[2] = {0, 0};
static unsigned long long tap_time_pre = 0;
static int touch_nr = 0, x_pre = 0, y_pre = 0;
static bool touch_cnt = true;
static bool registered;
static u64 last_wake_time;
static struct input_dev * wake_dev;
static DEFINE_MUTEX(pwrkeyworklock);
static struct work_struct s2w_input_work;
static struct work_struct s2s_input_work;
static struct work_struct dt2w_input_work;
static struct work_struct dt2s_input_work;
static struct wake_lock dt2w_wakelock;

void wg_setdev(struct input_dev * input_device) {
	wake_dev = input_device;
	return;
}

/* PowerKey work func */
static void wake_presspwr(struct work_struct * wake_presspwr_work) {
	if (!mutex_trylock(&pwrkeyworklock))
        	return;

	input_event(wake_dev, EV_KEY, KEY_POWER, 1);
	input_event(wake_dev, EV_SYN, 0, 0);
	msleep(WG_PWRKEY_DUR);
	input_event(wake_dev, EV_KEY, KEY_POWER, 0);
	input_event(wake_dev, EV_SYN, 0, 0);
	msleep(WG_PWRKEY_DUR);

	if (camera) {
		input_event(wake_dev, EV_KEY, KEY_POWER, 1);
		input_event(wake_dev, EV_SYN, 0, 0);
		msleep(WG_PWRKEY_DUR);
		input_event(wake_dev, EV_KEY, KEY_POWER, 0);
		input_event(wake_dev, EV_SYN, 0, 0);
		msleep(WG_PWRKEY_DUR);
		camera = false;
	}

    	mutex_unlock(&pwrkeyworklock);

	return;
}
static DECLARE_WORK(wake_presspwr_work, wake_presspwr);

/* PowerKey trigger */
static void wake_pwrtrigger(bool camera_trigger) {
	pwrtrigger_time[1] = pwrtrigger_time[0];
	pwrtrigger_time[0] = jiffies;

	if (pwrtrigger_time[0] - pwrtrigger_time[1] < TRIGGER_TIMEOUT)
		return;

	if (!camera_trigger)
		set_vibrate(vib_strength);
	else
		camera = true;

	schedule_work(&wake_presspwr_work);
        return;
}

/* Doubletap2wake */

static void doubletap2wake_reset(void) {
	if (wake_lock_active(&dt2w_wakelock))
		wake_unlock(&dt2w_wakelock);
	exec_count = true;
	touch_nr = 0;
	tap_time_pre = 0;
	x_pre = 0;
	y_pre = 0;
}

static unsigned int calc_feather(int coord, int prev_coord) {
	int calc_coord = 0;
	calc_coord = coord-prev_coord;
	if (calc_coord < 0)
		calc_coord = calc_coord * (-1);
	return calc_coord;
}

/* init a new touch */
static void new_touch(int x, int y) {
	tap_time_pre = jiffies;
	x_pre = x;
	y_pre = y;
	touch_nr++;
	wake_lock_timeout(&dt2w_wakelock, WAKE_TIMEOUT);
}

/* Doubletap2wake and Tap2wake main function */
static void detect_doubletap2wake(int x, int y, bool st, bool scr_suspended)
{
        bool single_touch = st;
	u64 now;

	now = ktime_to_us(ktime_get());
	if (timer_enabled && now - last_wake_time <= tap_wake_time) {
		return;
	}
/*Note that 3=Fullscreen 2=Downhalfscreen 3=Uphalfscreen*/
	if ((dt2w_switch == 3 && scr_suspended) || (t2w_switch == 3 && scr_suspended)) {
		if (x < SWEEP_EDGE || x > SWEEP_X_LIMIT)
			return;
		if (y < SWEEP_EDGE || y > SWEEP_Y_LIMIT)
			return;
		} else if ((dt2w_switch == 2 && scr_suspended) || (t2w_switch == 2 && scr_suspended)) {
			if (x < SWEEP_EDGE || x > DT2W_HS_X)
		       		return;
			if (y < SWEEP_EDGE || y > DT2W_HS_Y)
		       		return;
			} else if ((dt2w_switch == 1 && scr_suspended) || (t2w_switch == 1 && scr_suspended)) {
				if (x < SWEEP_EDGE || x > DT2W_HS_X)
			       		return;
				if (y < SWEEP_EDGE || y < DT2W_HS_Y)
			       		return;
				}

if (debug) {
        pr_info(LOGTAG"x,y(%4d,%4d) tap_time_pre:%llu\n",
                x, y, tap_time_pre);
}

	if ((scr_suspended) && (single_touch) && (dt2w_switch > 0) && (exec_count) && (touch_cnt)) {
		touch_cnt = false;
		if (touch_nr == 0) {
			new_touch(x, y);
		} else if (touch_nr == 1) {
			if ((calc_feather(x, x_pre) < dt2w_feather_x) &&
			    (calc_feather(y, y_pre) < dt2w_feather_y) &&
			    ((jiffies-tap_time_pre) < dt2w_time))
				touch_nr++;
			else {
				doubletap2wake_reset();
				new_touch(x, y);
			}
		} else {
			doubletap2wake_reset();
			new_touch(x, y);
		}
		if (touch_nr > 1) {
			exec_count = false;
			wake_pwrtrigger(false);
			if (timer_enabled) {
				last_wake_time = ktime_to_us(ktime_get());
			}
			doubletap2wake_reset();
		}
	} else if ((scr_suspended) && (single_touch) && (t2w_switch > 0) && (exec_count) && (touch_cnt)) {
		wake_pwrtrigger(false);
		if (timer_enabled) {
			last_wake_time = ktime_to_us(ktime_get());
		}
		doubletap2wake_reset();
	}
}

/* Doubletap2sleep main function */
static void detect_doubletap2sleep(int x, int y, bool st, bool scr_suspended)
{
        bool single_touch = st;
	u64 now;

	now = ktime_to_us(ktime_get());
	if (timer_enabled && now - last_wake_time <= tap_wake_time) {
		return;
	}
	if (!scr_suspended && x > dt2s_x)
   		return;
	if (!scr_suspended && y > dt2s_y)
   		return;

if (debug) {
        pr_info(LOGTAG"x,y(%4d,%4d) tap_time_pre:%llu\n",
                x, y, tap_time_pre);
}

   	if ((!scr_suspended) && (single_touch) && (dt2s_switch) && (exec_count) && (touch_cnt)) {
		touch_cnt = false;
		if (touch_nr == 0) {
			new_touch(x, y);
		} else if (touch_nr == 1) {
			if ((calc_feather(x, x_pre) < DT2S_FEATHER) &&
			    (calc_feather(y, y_pre) < DT2S_FEATHER) &&
			    ((jiffies-tap_time_pre) < dt2s_time))
				touch_nr++;
			else {
				doubletap2wake_reset();
				new_touch(x, y);
			}
		} else {
			doubletap2wake_reset();
			new_touch(x, y);
		}
		if ((touch_nr > 1)) {
			exec_count = false;
			wake_pwrtrigger(false);
			if (timer_enabled) {
				last_wake_time = ktime_to_us(ktime_get());
			}
			doubletap2wake_reset();
		}
	}
}

/* Sweep2wake/Sweep2sleep */

static void sweep2wake_reset(void) {

	exec_countx = true;
	barrierx[0] = false;
	barrierx[1] = false;
	firstx = 0;
	firstx_time = 0;

	exec_county = true;
	barriery[0] = false;
	barriery[1] = false;
	firsty = 0;
	firsty_time = 0;
}

/* Sweep2wake main functions*/

static void detect_sweep2wake(int x, int y, bool st, bool scr_suspended)
{
	int prevy = 0, nexty = 0;
        int prevx = 0, nextx = 0;
        bool single_touch = st;
	u64 now;

	now = ktime_to_us(ktime_get());
	if (timer_enabled && now - last_wake_time <= sweep_wake_time) {
		return;
	}
	if (scr_suspended && y > SWEEP_Y_LIMIT) {		
		sweep2wake_reset();
		return;
	}
	if (scr_suspended && x > SWEEP_X_LIMIT) {		
		sweep2wake_reset();
		return;
	}
	if (firsty == 0) {
		firsty = y;
		firsty_time = jiffies;
	}

if (debug) {
        pr_info(LOGTAG"s2w vert  x,y(%4d,%4d) single:%s\n",
                x, y, (single_touch) ? "true" : "false");
}

	//sweep up
	if ((scr_suspended) && (firsty > SWEEP_Y_START) && (single_touch) && (s2w_switch & SWEEP_UP)) {
		prevy = firsty;
		nexty = prevy - SWEEP_Y_NEXT;
		if (barriery[0] == true || (y < prevy && y > nexty)) {
			prevy = nexty;
			nexty -= SWEEP_Y_NEXT;
			barriery[0] = true;
			if (barriery[1] == true || (y < prevy && y > nexty)) {
				prevy = nexty;
				barriery[1] = true;
				if (y < prevy) {
					if (y < (nexty - SWEEP_Y_NEXT)) {
						if ((scr_suspended) && (exec_county) && (jiffies - firsty_time < SWEEP_TIMEOUT)) {
							wake_pwrtrigger(false);
							exec_county = false;
							if (timer_enabled) {
								last_wake_time = ktime_to_us(ktime_get());
							}
						}
					}
				}
			}
		}
	//sweep down
	} else if ((scr_suspended) && (firsty <= SWEEP_Y_START) && (single_touch) && (s2w_switch & SWEEP_DOWN || camera_switch)) {
		prevy = firsty;
		nexty = prevy + SWEEP_Y_NEXT;
		if (barriery[0] == true || (y > prevy && y < nexty)) {
			prevy = nexty;
			nexty += SWEEP_Y_NEXT;
			barriery[0] = true;
			if (barriery[1] == true || (y > prevy && y < nexty)) {
				prevy = nexty;
				barriery[1] = true;
				if (y > prevy) {
					if (y > (nexty + SWEEP_Y_NEXT)) {
						if ((scr_suspended) && (exec_county) && (jiffies - firsty_time < SWEEP_TIMEOUT)) {
							wake_pwrtrigger(camera_switch);
							exec_county = false;
							if (timer_enabled) {
								last_wake_time = ktime_to_us(ktime_get());
							}
						}
					}
				}
			}
		}
	}

	if (firstx == 0) {
		firstx = x;
		firstx_time = jiffies;
	}

if (debug) {
        pr_info(LOGTAG"s2w Horz x,y(%4d,%4d) wake:%s\n",
                x, y, (scr_suspended) ? "true" : "false");
}

	//left->right
	if ((scr_suspended) && (firstx < SWEEP_X_START) && (single_touch) && (s2w_switch & SWEEP_RIGHT)) {
		prevx = 0;
		nextx = SWEEP_X_B1;
		if ((barrierx[0] == true) ||
		   ((x > prevx) && (x < nextx))) {
			prevx = nextx;
			nextx = SWEEP_X_B2;
			barrierx[0] = true;
			if ((barrierx[1] == true) ||
			   ((x > prevx) && (x < nextx))) {
				prevx = nextx;
				barrierx[1] = true;
				if (x > prevx) {
					if (x > (SWEEP_X_MAX - SWEEP_X_FINAL)) {
						if ((scr_suspended) && (exec_countx) && (jiffies - firstx_time < SWEEP_TIMEOUT)) {
							wake_pwrtrigger(false);
							exec_countx = false;
							if (timer_enabled) {
								last_wake_time = ktime_to_us(ktime_get());
							}
						}
					}
				}
			}
		}
	//right->left
	} else if ((scr_suspended) && (firstx >= SWEEP_X_START) && (single_touch) && (s2w_switch & SWEEP_LEFT)) {
		prevx = (SWEEP_X_MAX - SWEEP_X_FINAL);
		nextx = SWEEP_X_B2;
		if ((barrierx[0] == true) ||
		   ((x < prevx) && (x > nextx))) {
			prevx = nextx;
			nextx = SWEEP_X_B1;
			barrierx[0] = true;
			if ((barrierx[1] == true) ||
			   ((x < prevx) && (x > nextx))) {
				prevx = nextx;
				barrierx[1] = true;
				if (x < prevx) {
					if (x < (SWEEP_X_MAX - SWEEP_X_FINAL)) {
						if ((scr_suspended) && (exec_countx) && (jiffies - firstx_time < SWEEP_TIMEOUT)) {
							wake_pwrtrigger(false);
							exec_countx = false;
							if (timer_enabled) {
								last_wake_time = ktime_to_us(ktime_get());
							}
						}
					}
				}
			}
		}
	}
}

static void detect_sweep2sleep(int x, int y, bool st, bool scr_suspended)
{
        int prevx = 0, nextx = 0;
        bool single_touch = st;
	u64 now;

	now = ktime_to_us(ktime_get());
	if (timer_enabled && now - last_wake_time <= sweep_wake_time) {
		return;
	}
	if (!scr_suspended && y < SWEEP_Y_LIMIT) {
		sweep2wake_reset();		
		return;
	}
	if (firstx == 0) {
		firstx = x;
		firstx_time = jiffies;
	}

if (debug) {
        pr_info(LOGTAG"s2s Horz x,y(%4d,%4d) wake:%s\n",
                x, y, (scr_suspended) ? "true" : "false");
}

	//left->right
	if ((!scr_suspended) && (firstx < SWEEP_X_START) && (single_touch) && (s2s_switch & SWEEP_RIGHT)) {
		prevx = 0;
		nextx = SWEEP_X_B1;
		if ((barrierx[0] == true) ||
		   ((x > prevx) && (x < nextx))) {
			prevx = nextx;
			nextx = SWEEP_X_B2;
			barrierx[0] = true;
			if ((barrierx[1] == true) ||
			   ((x > prevx) && (x < nextx))) {
				prevx = nextx;
				barrierx[1] = true;
				if (x > prevx) {
					if (x > (SWEEP_X_MAX - SWEEP_X_FINAL)) {
						if ((!scr_suspended) && (exec_countx) && (jiffies - firstx_time < SWEEP_TIMEOUT)) {
							wake_pwrtrigger(false);
							exec_countx = false;
							if (timer_enabled) {
								last_wake_time = ktime_to_us(ktime_get());
							}
						}
					}
				}
			}
		}
	//right->left
	} else if ((!scr_suspended) && (firstx >= SWEEP_X_START) && (single_touch) && (s2s_switch & SWEEP_LEFT)) {
		prevx = (SWEEP_X_MAX - SWEEP_X_FINAL);
		nextx = SWEEP_X_B2;
		if ((barrierx[0] == true) ||
		   ((x < prevx) && (x > nextx))) {
			prevx = nextx;
			nextx = SWEEP_X_B1;
			barrierx[0] = true;
			if ((barrierx[1] == true) ||
			   ((x < prevx) && (x > nextx))) {
				prevx = nextx;
				barrierx[1] = true;
				if (x < prevx) {
					if (x < (SWEEP_X_MAX - SWEEP_X_FINAL)) {
						if ((!scr_suspended) && (exec_countx) && (jiffies - firstx_time < SWEEP_TIMEOUT)) {
							wake_pwrtrigger(false);
							exec_countx = false;
							if (timer_enabled) {
								last_wake_time = ktime_to_us(ktime_get());
							}
						}
					}
				}
			}
		}
	}
}

static void s2w_input_callback(struct work_struct *unused)
{
	if (scr_suspended() && s2w_switch > 0)
		detect_sweep2wake(touch_x, touch_y, true, scr_suspended());
		return;
}

static void s2s_input_callback(struct work_struct *unused)
{
	if (!scr_suspended() && s2s_switch > 0)
		detect_sweep2sleep(touch_x, touch_y, true, scr_suspended());
		return;
}

static void dt2w_input_callback(struct work_struct *unused)
{
	if ((scr_suspended() && dt2w_switch > 0) || (scr_suspended() && t2w_switch > 0))
		detect_doubletap2wake(touch_x, touch_y, true, scr_suspended());
		return;
}

static void dt2s_input_callback(struct work_struct *unused)
{
	if (!scr_suspended() && dt2s_switch > 0)
		detect_doubletap2sleep(touch_x, touch_y, true, scr_suspended());
		return;
}

static void wg_input_event(struct input_handle *handle, unsigned int type,
				unsigned int code, int value)
{
	if (scr_suspended() && code == ABS_MT_POSITION_X) {
		value -= 5000;
	}
if (debug) {
	pr_info("wg: code: %s|%u, val: %i\n",
		((code==ABS_MT_POSITION_X) ? "X" :
		(code==ABS_MT_POSITION_Y) ? "Y" :
		(code==ABS_MT_TRACKING_ID) ? "ID" :
		"undef"), code, value);
}

	if (code == ABS_MT_SLOT) {
		sweep2wake_reset();
		doubletap2wake_reset();
		return;
	}

	if (code == ABS_MT_TRACKING_ID && value == -1) {
		sweep2wake_reset();
		touch_cnt = true;
		if (scr_suspended()) {
			schedule_work(&dt2w_input_work);
		} else if (!scr_suspended()) {
			schedule_work(&dt2s_input_work);
		}
		return;
	}

	if (code == ABS_MT_POSITION_X) {
		touch_x = value;
		touch_x_called = true;
	}

	if (code == ABS_MT_POSITION_Y) {
		touch_y = value;
		touch_y_called = true;
	}

	if (touch_x_called && touch_y_called) {
		touch_x_called = false;
		touch_y_called = false;
		if (scr_suspended()) {
			schedule_work(&s2w_input_work);
		} else if (!scr_suspended()) {
			schedule_work(&s2s_input_work);
		}
	} else if (touch_x_called && !touch_y_called) {
		touch_x_called = false;
		touch_y_called = false;
		if (scr_suspended()) {
			schedule_work(&s2w_input_work);
		} else if (!scr_suspended()) {
			schedule_work(&s2s_input_work);
		}
	}
}

static int input_dev_filter(struct input_dev *dev) {
	if (strstr(dev->name, "atmel_mxt_ts")) {
		return 0;
	} else {
		return 1;
	}
	return 0;
}

static int wg_input_connect(struct input_handler *handler,
				struct input_dev *dev, const struct input_device_id *id) {
	struct input_handle *handle;
	int error;

	if (input_dev_filter(dev))
		return -ENODEV;

	handle = kzalloc(sizeof(struct input_handle), GFP_KERNEL);
	if (!handle)
		return -ENOMEM;

	handle->dev = dev;
	handle->handler = handler;
	handle->name = "wg";

	error = input_register_handle(handle);
	if (error)
		goto err2;

	error = input_open_device(handle);
	if (error)
		goto err1;

	return 0;
err1:
	input_unregister_handle(handle);
err2:
	kfree(handle);
	return error;
}

static void wg_input_disconnect(struct input_handle *handle) {
	input_close_device(handle);
	input_unregister_handle(handle);
	kfree(handle);
}

static const struct input_device_id wg_ids[] = {
	{ .driver_info = 1 },
	{ },
};

static struct input_handler wg_input_handler = {
	.event		= wg_input_event,
	.connect	= wg_input_connect,
	.disconnect	= wg_input_disconnect,
	.name		= "wg_inputreq",
	.id_table	= wg_ids,
};

static void wk_power_suspend(struct power_suspend *h)
{
	if (gesture_suspend) {
		if (timer_enabled)
			last_wake_time = ktime_to_us(ktime_get());
		if (debug)
			pr_info(LOGTAG"suspend\n");
	}
}

static void wk_power_resume(struct power_suspend *h)
{
	if (gesture_suspend) {
		if (timer_enabled)
			last_wake_time = ktime_to_us(ktime_get());
		if (debug)
			pr_info(LOGTAG"resume\n");
	}
}

static struct power_suspend wk_power_suspend_handler = {
	.suspend = wk_power_suspend,
	.resume = wk_power_resume,
};

static void unregister_wg(void)
{
	if (!registered)
		return;

	registered = false;
	wake_lock_destroy(&dt2w_wakelock);
	input_unregister_handler(&wg_input_handler);

	cancel_work_sync(&s2w_input_work);
	cancel_work_sync(&s2s_input_work);
	cancel_work_sync(&dt2w_input_work);
	cancel_work_sync(&dt2s_input_work);
}

static int register_wg(void)
{
	int rc = 0;

	if ((!s2w_switch) && (!s2s_switch) && (!t2w_switch) && (!dt2w_switch) && (!dt2s_switch) && (!camera_switch)) {
		unregister_wg();
		return rc;
	}

	if (registered)
		return rc;

	INIT_WORK(&s2w_input_work, s2w_input_callback);
	INIT_WORK(&s2s_input_work, s2s_input_callback);
	INIT_WORK(&dt2w_input_work, dt2w_input_callback);
	INIT_WORK(&dt2s_input_work, dt2s_input_callback);
	wake_lock_init(&dt2w_wakelock, WAKE_LOCK_SUSPEND, "dt2w_wakelock");

	rc = input_register_handler(&wg_input_handler);
	if (rc) {
		pr_err("%s: Failed to register wg_input_handler\n", __func__);
		goto err;
	}

	registered = true;

	return rc;
err:
	wake_lock_destroy(&dt2w_wakelock);
	return rc;
}

/*
 * SYSFS stuff below here
 */
static ssize_t debug_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;

	count += sprintf(buf, "%d\n", debug);

	return count;
}

static ssize_t debug_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ", &debug);
	if (debug < 0 || debug > 1)
		debug = 0;

	return count;
}

static DEVICE_ATTR(debug, (S_IWUSR|S_IRUGO),
	debug_show, debug_dump);

static ssize_t sweep2wake_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;

	count += sprintf(buf, "%d\n", s2w_switch);

	return count;
}

static ssize_t sweep2wake_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ", &s2w_switch);
	if (s2w_switch < 0 || s2w_switch > 15)
		s2w_switch = 0;

	if (s2w_switch == 0 && camera_switch == 0 && dt2w_switch)
		set_internal_dt(true);
	else {
		set_internal_dt(false);
	}

	register_wg();
	return count;
}

static DEVICE_ATTR(sweep2wake, (S_IWUSR|S_IRUGO),
	sweep2wake_show, sweep2wake_dump);

static ssize_t sweep2sleep_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;
	count += sprintf(buf, "%d\n", s2s_switch);
	return count;
}

static ssize_t sweep2sleep_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ", &s2s_switch);
	if (s2s_switch < 0 || s2s_switch > 3)
		s2s_switch = 0;				

	register_wg();
	return count;
}

static DEVICE_ATTR(sweep2sleep, (S_IWUSR|S_IRUGO),
	sweep2sleep_show, sweep2sleep_dump);

static ssize_t tap2wake_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;

	count += sprintf(buf, "%d\n", t2w_switch);

	return count;
}

static ssize_t tap2wake_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ", &t2w_switch);
	if (t2w_switch < 0 || t2w_switch > 3)
		t2w_switch = 0;
	if (t2w_switch > 0)
		dt2w_switch = 0;

	register_wg();
	return count;
}

static DEVICE_ATTR(tap2wake, (S_IWUSR|S_IRUGO),
	tap2wake_show, tap2wake_dump);

static ssize_t doubletap2wake_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;

	count += sprintf(buf, "%d\n", dt2w_switch);

	return count;
}

static ssize_t doubletap2wake_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ", &dt2w_switch);
	if (dt2w_switch < 0 || dt2w_switch > 3)
		dt2w_switch = 0;
	if (dt2w_switch > 0)
		t2w_switch = 0;

	if (s2w_switch == 0 && camera_switch == 0 && dt2w_switch)
		set_internal_dt(true);
	else if (dt2w_switch == 0) {
		set_internal_dt(false);
	}

	register_wg();
	return count;
}

static DEVICE_ATTR(doubletap2wake, (S_IWUSR|S_IRUGO),
	doubletap2wake_show, doubletap2wake_dump);

static ssize_t doubletap2sleep_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;

	count += sprintf(buf, "%d\n", dt2s_switch);

	return count;
}

static ssize_t doubletap2sleep_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{

	sscanf(buf, "%d ", &dt2s_switch);
	if (dt2s_switch < 0 || dt2s_switch > 1)
		dt2s_switch = 0;			

	register_wg();
	return count;
}

static DEVICE_ATTR(doubletap2sleep, (S_IWUSR|S_IRUGO),
	doubletap2sleep_show, doubletap2sleep_dump);

static ssize_t doubletap2sleep_x_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;
	count += sprintf(buf, "%d\n", dt2s_x);
	return count;
}
static ssize_t doubletap2sleep_x_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ", &dt2s_x);
	return count;
}

static DEVICE_ATTR(doubletap2sleep_x, (S_IWUSR|S_IRUGO),
	doubletap2sleep_x_show, doubletap2sleep_x_dump);

static ssize_t doubletap2sleep_y_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;
	count += sprintf(buf, "%d\n", dt2s_y);
	return count;
}
static ssize_t doubletap2sleep_y_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ", &dt2s_y);
	return count;
}

static DEVICE_ATTR(doubletap2sleep_y, (S_IWUSR|S_IRUGO),
	doubletap2sleep_y_show, doubletap2sleep_y_dump);

static ssize_t camera_gesture_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;

	count += sprintf(buf, "%d\n", camera_switch);

	return count;
}

static ssize_t camera_gesture_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ", &camera_switch);
	if (camera_switch < 0 || camera_switch > 1)
		camera_switch = 0;
	if (s2w_switch == 0)
		camera_switch = 0;

	if (s2w_switch == 0 && camera_switch == 0 && dt2w_switch)
		set_internal_dt(true);
	else {
		set_internal_dt(false);
	}

	register_wg();
	return count;
}

static DEVICE_ATTR(camera_gesture, (S_IWUSR|S_IRUGO),
	camera_gesture_show, camera_gesture_dump);

static ssize_t vib_strength_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;
	count += sprintf(buf, "%d\n", vib_strength);
	return count;
}

static ssize_t vib_strength_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ",&vib_strength);
	if (vib_strength < 0 || vib_strength > 90)
		vib_strength = 44;

	return count;
}

static DEVICE_ATTR(vib_strength, (S_IWUSR|S_IRUGO),
	vib_strength_show, vib_strength_dump);

static ssize_t gesture_suspend_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;
	count += sprintf(buf, "%d\n", gesture_suspend);
	return count;
}

static ssize_t gesture_suspend_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ",&gesture_suspend);
	if (gesture_suspend < 0 || gesture_suspend > 1)
		gesture_suspend = 0;

	return count;
}

static DEVICE_ATTR(gesture_suspend, (S_IWUSR|S_IRUGO),
	gesture_suspend_show, gesture_suspend_dump);

static ssize_t dt2w_time_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;
	count += sprintf(buf, "%d\n", dt2w_time);
	return count;
}

static ssize_t dt2w_time_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ",&dt2w_time);
	if (dt2w_time < 25 || dt2w_time > 400)
		dt2w_time = 200;

	return count;
}

static DEVICE_ATTR(dt2w_time, (S_IWUSR|S_IRUGO),
	dt2w_time_show, dt2w_time_dump);

static ssize_t dt2s_time_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;
	count += sprintf(buf, "%d\n", dt2s_time);
	return count;
}

static ssize_t dt2s_time_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ",&dt2s_time);
	if (dt2s_time < 25 || dt2s_time > 400)
		dt2s_time = 200;

	return count;
}

static DEVICE_ATTR(dt2s_time, (S_IWUSR|S_IRUGO),
	dt2s_time_show, dt2s_time_dump);

static ssize_t dt2w_feather_x_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;
	count += sprintf(buf, "%d\n", dt2w_feather_x);
	return count;
}

static ssize_t dt2w_feather_x_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ",&dt2w_feather_x);
	if (dt2w_feather_x < 1 || dt2w_feather_x > 600)
		dt2w_feather_x = 200;

	return count;
}

static DEVICE_ATTR(dt2w_feather_x, (S_IWUSR|S_IRUGO),
	dt2w_feather_x_show, dt2w_feather_x_dump);

static ssize_t dt2w_feather_y_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;
	count += sprintf(buf, "%d\n", dt2w_feather_y);
	return count;
}

static ssize_t dt2w_feather_y_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ",&dt2w_feather_y);
	if (dt2w_feather_y < 1 || dt2w_feather_y > 600)
		dt2w_feather_y = 200;

	return count;
}

static DEVICE_ATTR(dt2w_feather_y, (S_IWUSR|S_IRUGO),
	dt2w_feather_y_show, dt2w_feather_y_dump);

static ssize_t timer_enabled_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	size_t count = 0;
	count += sprintf(buf, "%d\n", timer_enabled);
	return count;
}

static ssize_t timer_enabled_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d ",&timer_enabled);
	if (timer_enabled < 0 || timer_enabled > 1)
		timer_enabled = 0;

	return count;
}

static DEVICE_ATTR(timer_enabled, (S_IWUSR|S_IRUGO),
	timer_enabled_show, timer_enabled_dump);

static ssize_t tap_wake_time_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%llu\n", div_u64(tap_wake_time, 1000));
}

static ssize_t tap_wake_time_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	int ret;
	u64 val;

	ret = sscanf(buf, "%llu", &val);
	if (ret != 1)
		return -EINVAL;

	tap_wake_time = val * 1000;

	return count;
}

static DEVICE_ATTR(tap_wake_time, (S_IWUSR|S_IRUGO),
	tap_wake_time_show, tap_wake_time_dump);

static ssize_t sweep_wake_time_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%llu\n", div_u64(sweep_wake_time, 1000));
}

static ssize_t sweep_wake_time_dump(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	int ret;
	u64 val;

	ret = sscanf(buf, "%llu", &val);
	if (ret != 1)
		return -EINVAL;

	sweep_wake_time = val * 1000;

	return count;
}

static DEVICE_ATTR(sweep_wake_time, (S_IWUSR|S_IRUGO),
	sweep_wake_time_show, sweep_wake_time_dump);

/*
 * INIT / EXIT stuff below here
 */

struct kobject *android_touch_kobj;
EXPORT_SYMBOL_GPL(android_touch_kobj);

static int __init wake_gestures_init(void)
{
	int rc = 0;

	dt2w_switch = get_internal_dt();

	rc = register_wg();
	register_power_suspend(&wk_power_suspend_handler);

	android_touch_kobj = kobject_create_and_add("android_touch", NULL) ;
	if (android_touch_kobj == NULL) {
		pr_warn("%s: android_touch_kobj create_and_add failed\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_debug.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for debug\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_sweep2wake.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for sweep2wake\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_sweep2sleep.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for sweep2sleep\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_tap2wake.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for tap2wake\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_doubletap2wake.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for doubletap2wake\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_doubletap2sleep.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for doubletap2sleep\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_doubletap2sleep_x.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for doubletap2sleep_y\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_doubletap2sleep_y.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for doubletap2sleep_y\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_vib_strength.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for vib_strength\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_camera_gesture.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for camera_gesture\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_gesture_suspend.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for gesture_suspend\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_dt2w_time.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for dt2w time\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_dt2s_time.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for dt2w time\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_dt2w_feather_x.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for dt2w feather x\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_dt2w_feather_y.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for dt2w feather y\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_timer_enabled.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for timer enabled\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_tap_wake_time.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for tap wake time\n", __func__);
	}
	rc = sysfs_create_file(android_touch_kobj, &dev_attr_sweep_wake_time.attr);
	if (rc) {
		pr_warn("%s: sysfs_create_file failed for sweep wake time\n", __func__);
	}

	return 0;
}

static void __exit wake_gestures_exit(void)
{
	kobject_del(android_touch_kobj);
	input_free_device(wake_dev);
	unregister_wg();
	unregister_power_suspend(&wk_power_suspend_handler);

	return;
}

module_init(wake_gestures_init);
module_exit(wake_gestures_exit);
