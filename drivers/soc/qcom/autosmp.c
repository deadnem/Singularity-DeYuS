/*
 * HOTPLUG autosmp.c
 *
 * automatically hotplug/unplug multiple cpu cores
 * based on cpu load and suspend state
 *
 * based on the msm_mpdecision code by
 * Copyright (c) 2012-2013, Dennis Rassmann <showp1984@gmail.com>
 *
 * Copyright (C) 2013-2014, Rauf Gungor, http://github.com/mrg666
 * rewrite to simplify and optimize, Jul. 2013, http://goo.gl/cdGw6x
 * optimize more, generalize for n cores, Sep. 2013, http://goo.gl/448qBz
 * generalize for all arch, rename as autosmp, Dec. 2013, http://goo.gl/x5oyhy
 *
 *
 * Neobuddy89 2017
 * He add touchboost work
 *
 * DeadneM deadnem@gmail.com 2017
 * I've add some good stuff !
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. For more details, see the GNU
 * General Public License included with the Linux kernel or available
 * at www.gnu.org/licenses
 */

#include <linux/moduleparam.h>
#include <linux/cpufreq.h>
#include <linux/workqueue.h>
#include <linux/cpu.h>
#include <linux/cpumask.h>
#include <linux/hrtimer.h>
#include <linux/slab.h>
#include <linux/input.h>
#include <linux/hotplug_state.h>
#include <linux/powersuspend.h>

#define ASMP_TAG		"AutoSMP:"
#define DEBUG_DEFAULT		0
#define ENABLED_DEFAULT		1
#define DELAY_DEFAULT		25
#define HOTPLUG_S_DEFAULT	1		
#define MAX_CPUS_DEFAULT	4
#define MIN_CPUS_DEFAULT	1
#define MINMAX_SCROFF_DEFAULT	2
#define CPUFREQ_UP_DEFAULT	85
#define CPUFREQ_DOWN_DEFAULT	75
#define CYCLE_UP_DEFAULT	2
#define CYCLE_DOWN_DEFAULT	4
#define BOOST_FREQ_DEFAULT	1728000
#define CPU_BOOST_DEFAULT	2
#define BOOST_TIME_DEFAULT	1250 * 1000L
#define INPUT_ITVL_DEFAULT	500 * 1000L
static unsigned int enabled = ENABLED_DEFAULT;
static struct delayed_work asmp_work;
static struct workqueue_struct *asmp_workq;
static DEFINE_PER_CPU(struct asmp_cpudata_t, asmp_cpudata);
static unsigned int suspend = 0;
static unsigned int suspend_pause = 0;
static unsigned int bcl_hotplug = 0;
unsigned int cpu_up_count = 0;
unsigned int cpu_down_count = 0;
static u64 boost_time = BOOST_TIME_DEFAULT;
static u64 input_interval = INPUT_ITVL_DEFAULT;
static u64 last_boost_time;
static struct asmp_param_struct {
	unsigned int debug;
	unsigned int delay;
	unsigned int hotplug_suspend;
	unsigned int max_cpus;
	unsigned int min_cpus;
	unsigned int minmax_cpus_scroff;
	unsigned int cpufreq_up;
	unsigned int cpufreq_down;
	unsigned int cycle_up;
	unsigned int cycle_down;
	unsigned int boost_freq;
	unsigned int cpu_boost;
} asmp_param = {
	.debug = DEBUG_DEFAULT,
	.delay = DELAY_DEFAULT,
	.hotplug_suspend = HOTPLUG_S_DEFAULT,
	.max_cpus = MAX_CPUS_DEFAULT,
	.min_cpus = MIN_CPUS_DEFAULT,
	.minmax_cpus_scroff = MINMAX_SCROFF_DEFAULT,
	.cpufreq_up = CPUFREQ_UP_DEFAULT,
	.cpufreq_down = CPUFREQ_DOWN_DEFAULT,
	.cycle_up = CYCLE_UP_DEFAULT,
	.cycle_down = CYCLE_DOWN_DEFAULT,
	.boost_freq = BOOST_FREQ_DEFAULT,
	.cpu_boost = CPU_BOOST_DEFAULT,
};

static void asmp_work_loop(void)
{
	if ((suspend) || (bcl_hotplug)) {
		if ((suspend_pause) || (!bcl_hotplug)) {
			queue_delayed_work(asmp_workq, &asmp_work,
					msecs_to_jiffies(asmp_param.delay*10));
		} else {
			queue_delayed_work(asmp_workq, &asmp_work,
					msecs_to_jiffies(asmp_param.delay*100));
		}
	} else if (!suspend) {
		queue_delayed_work(asmp_workq, &asmp_work,
				msecs_to_jiffies(asmp_param.delay));
	}
}

static void __ref asmp_upall(void)
{
	unsigned int cpu = 0;
	for_each_present_cpu(cpu) {
		if (num_online_cpus() >= nr_cpu_ids)
			break;
		if (!cpu_online(cpu))
			cpu_up(cpu);
	}
}

static void __ref asmp_work_fn(struct work_struct *work)
{
	unsigned int cpu = 0, slow_cpu = 0;
	unsigned int rate, cpu0_rate, slow_rate = UINT_MAX, fast_rate;
	unsigned int max_rate, up_rate, down_rate;
	unsigned int nr_cpu_online;
	const bool hotplug_state = is_hotplug_state();
	u64 now;

	if (hotplug_state) {
		/* Make hotplug work loop in bcl active state. Up cpu one time to make sure cpu0 and cpu1 are on */
		if (!bcl_hotplug) {
			asmp_upall();
		}
		bcl_hotplug = 1;
		if (asmp_param.debug) {
			pr_info(ASMP_TAG"bcl active\n");
		}
		goto loop;
	} else {
		bcl_hotplug = 0;
		nr_cpu_online = num_online_cpus();

		if ((suspend) && (nr_cpu_online == asmp_param.minmax_cpus_scroff)) {
			if (asmp_param.debug) {
				pr_info(ASMP_TAG"paused\n");
			}
			suspend_pause = 1;
			goto loop;
		} else {
			suspend_pause = 0;

			/* get maximum possible freq for cpu0 and
			   calculate up/down limits */
			max_rate  = cpufreq_quick_get_max(cpu);
			up_rate   = (max_rate / 100) * asmp_param.cpufreq_up;
			down_rate = (max_rate / 100) * asmp_param.cpufreq_down;

			/* find current max and min cpu freq to estimate load */
			cpu0_rate = cpufreq_quick_get(cpu);
			fast_rate = cpu0_rate;
			for_each_online_cpu(cpu) {
				if (cpu) {
					rate = cpufreq_quick_get(cpu);
					if (rate <= slow_rate) {
						slow_cpu = cpu;
						slow_rate = rate;
					} else if (rate > fast_rate)
						fast_rate = rate;
				}
			}
			if (cpu0_rate < slow_rate)
				slow_rate = cpu0_rate;
			if (max_rate <= asmp_param.boost_freq)
				 asmp_param.boost_freq = max_rate;

			now = ktime_to_us(ktime_get());
			/* hotplug one core if all online cores are over up_rate limit */
			if (slow_rate > up_rate && fast_rate >= asmp_param.boost_freq) {
				cpu_up_count++;
				if ((nr_cpu_online < asmp_param.max_cpus) &&
				    (cpu_up_count >= asmp_param.cycle_up)) {
					cpu = cpumask_next_zero(0, cpu_online_mask);
					if (cpu_is_offline(cpu))
						cpu_up(cpu);
					cpu_up_count = 0;
					if (asmp_param.debug) {
						pr_info_ratelimited(ASMP_TAG"CPU [%d] On  | Mask [%d%d%d%d]\n",
						    cpu, cpu_online(0), cpu_online(1), cpu_online(2), cpu_online(3));
					}
				}
			/* check if boost required */
			} else if ((!suspend) && (nr_cpu_online < asmp_param.cpu_boost) &&
					(now - last_boost_time <= boost_time)) {
				if (nr_cpu_online < asmp_param.cpu_boost &&
					nr_cpu_online < asmp_param.max_cpus) {
					cpu = cpumask_next_zero(0, cpu_online_mask);
					if (cpu_is_offline(cpu))
						cpu_up(cpu);
					cpu_up_count = 0;
					cpu_down_count = 0;
					if (asmp_param.debug) {
						pr_info_ratelimited(ASMP_TAG"CPU [%d] On/Boosted  | Mask [%d%d%d%d]\n",
						    cpu, cpu_online(0), cpu_online(1), cpu_online(2), cpu_online(3));
					}
				}
			/* unplug slowest core if all online cores are under down_rate limit */
			} else if ((slow_cpu) && (fast_rate < down_rate)) {
				cpu_down_count++;
				if ((nr_cpu_online > asmp_param.min_cpus) &&
				    (cpu_down_count >= asmp_param.cycle_down)) {
					if (cpu_online(slow_cpu))
						cpu_down(slow_cpu);
					cpu_down_count = 0;
					if (asmp_param.debug) {
						pr_info_ratelimited(ASMP_TAG"CPU [%d] Off | Mask [%d%d%d%d]\n",
						    slow_cpu, cpu_online(0), cpu_online(1), cpu_online(2), cpu_online(3));
					}
				}
			}
goto loop;
		}
	}
loop:
	asmp_work_loop();
}

static void asmp_early_suspend(struct power_suspend *handler)
{
	if ((!bcl_hotplug) && (asmp_param.hotplug_suspend)) {
		suspend = 1;
		if (asmp_param.minmax_cpus_scroff < asmp_param.min_cpus) {
			asmp_param.minmax_cpus_scroff = asmp_param.min_cpus;
		}
		if (asmp_param.debug) {
			pr_info(ASMP_TAG"suspended\n");
		}
	} else if ((!bcl_hotplug) && (!asmp_param.hotplug_suspend) && (!suspend)) {
		suspend = 1;
		flush_workqueue(asmp_workq);
		cancel_delayed_work_sync(&asmp_work);
		asmp_upall();
		if (asmp_param.debug) {
			pr_info(ASMP_TAG"stopped\n");
		}
	}
}

static void asmp_late_resume(struct power_suspend *handler)
{
	if ((!bcl_hotplug) && (asmp_param.hotplug_suspend)) {
		suspend = 0;
		if (asmp_param.debug) {
			pr_info(ASMP_TAG"resumed\n");
		}
	} else if ((!bcl_hotplug) && (!asmp_param.hotplug_suspend) && (suspend)) {
		suspend = 0;
		INIT_DELAYED_WORK(&asmp_work, asmp_work_fn);
		asmp_work_loop();
		if (asmp_param.debug) {
			pr_info(ASMP_TAG"restarted\n");
		}
	}
}

static struct power_suspend asmp_power_suspend_handler = {
	.suspend = asmp_early_suspend,
	.resume = asmp_late_resume,
};

static void autosmp_input_event(struct input_handle *handle, unsigned int type,
				unsigned int code, int value)
{
	u64 now;

	if ((!enabled) || (!asmp_param.cpu_boost))
		return;

	if (suspend)
		return;

	now = ktime_to_us(ktime_get());
	if (now - last_boost_time < input_interval)
		return;

	if (asmp_param.cpu_boost <= asmp_param.min_cpus)
		return;

	last_boost_time = ktime_to_us(ktime_get());
}

static int autosmp_input_connect(struct input_handler *handler,
				 struct input_dev *dev,
				 const struct input_device_id *id)
{
	struct input_handle *handle;
	int err;

	handle = kzalloc(sizeof(struct input_handle), GFP_KERNEL);
	if (!handle)
		return -ENOMEM;

	handle->dev = dev;
	handle->handler = handler;
	handle->name = handler->name;

	err = input_register_handle(handle);
	if (err)
		goto err_register;

	err = input_open_device(handle);
	if (err)
		goto err_open;

	return 0;
err_open:
	input_unregister_handle(handle);
err_register:
	kfree(handle);
	return err;
}

static void autosmp_input_disconnect(struct input_handle *handle)
{
	input_close_device(handle);
	input_unregister_handle(handle);
	kfree(handle);
}

static const struct input_device_id autosmp_ids[] = {
	/* multi-touch touchscreen */
	{
		.flags = INPUT_DEVICE_ID_MATCH_EVBIT |
			INPUT_DEVICE_ID_MATCH_ABSBIT,
		.evbit = { BIT_MASK(EV_ABS) },
		.absbit = { [BIT_WORD(ABS_MT_POSITION_X)] =
			BIT_MASK(ABS_MT_POSITION_X) |
			BIT_MASK(ABS_MT_POSITION_Y) },
	},
	/* touchpad */
	{
		.flags = INPUT_DEVICE_ID_MATCH_KEYBIT |
			INPUT_DEVICE_ID_MATCH_ABSBIT,
		.keybit = { [BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH) },
		.absbit = { [BIT_WORD(ABS_X)] =
			BIT_MASK(ABS_X) | BIT_MASK(ABS_Y) },
	},
	/* Keypad */
	{
		.flags = INPUT_DEVICE_ID_MATCH_EVBIT,
		.evbit = { BIT_MASK(EV_KEY) },
	},
	{ },
};

static struct input_handler autosmp_input_handler = {
	.event		= autosmp_input_event,
	.connect	= autosmp_input_connect,
	.disconnect	= autosmp_input_disconnect,
	.name		= ASMP_TAG,
	.id_table	= autosmp_ids,
};

static int asmp_start(void)
{
	int ret = 0;
	asmp_workq = alloc_workqueue("asmp", WQ_UNBOUND | WQ_HIGHPRI | WQ_FREEZABLE, 0);
	if (!asmp_workq)
		return -ENOMEM;
	ret = input_register_handler(&autosmp_input_handler);
	if (ret)
		pr_err("%s: Failed to register input handler: %d\n",
		       ASMP_TAG, ret);
	register_power_suspend(&asmp_power_suspend_handler);
	INIT_DELAYED_WORK(&asmp_work, asmp_work_fn);
	asmp_work_loop();
	if (asmp_param.debug) {
		pr_info(ASMP_TAG"enabled\n");
	}
	return ret;
}

static void asmp_stop(void)
{
	unsigned int cpu = 0;
	input_unregister_handler(&autosmp_input_handler);
	flush_workqueue(asmp_workq);
	cancel_delayed_work_sync(&asmp_work);
	destroy_workqueue(asmp_workq);
	unregister_power_suspend(&asmp_power_suspend_handler);
	asmp_upall();
	if (asmp_param.debug) {
		pr_info(ASMP_TAG"disabled\n");
	}
}

/***************************** SYSFS START *****************************/
#define define_one_global_rw(_name)					\
static struct global_attr _name =					\
__ATTR(_name, 0644, show_##_name, store_##_name)
struct kobject *asmp_kobject;
#define show_one(file_name, object)					\
static ssize_t show_##file_name						\
(struct kobject *kobj, struct attribute *attr, char *buf)		\
{									\
	return sprintf(buf, "%u\n", asmp_param.object);			\
}
show_one(debug, debug);
show_one(delay, delay);
show_one(hotplug_suspend, hotplug_suspend);
show_one(max_cpus, max_cpus);
show_one(min_cpus, min_cpus);
show_one(minmax_cpus_scroff, minmax_cpus_scroff);
show_one(cpufreq_up, cpufreq_up);
show_one(cpufreq_down, cpufreq_down);
show_one(cycle_up, cycle_up);
show_one(cycle_down, cycle_down);
show_one(cpu_boost, cpu_boost);
show_one(boost_freq, boost_freq);
#define store_one(file_name, object)					\
static ssize_t store_##file_name					\
(struct kobject *a, struct attribute *b, const char *buf, size_t count)	\
{									\
	unsigned int input;						\
	int ret;							\
	ret = sscanf(buf, "%u", &input);				\
	if (ret != 1)							\
		return -EINVAL;						\
	asmp_param.object = input;					\
	return count;							\
}									\
define_one_global_rw(file_name);
store_one(debug, debug);
store_one(delay, delay);
store_one(hotplug_suspend, hotplug_suspend);
store_one(max_cpus, max_cpus);
store_one(min_cpus, min_cpus);
store_one(minmax_cpus_scroff, minmax_cpus_scroff);
store_one(cpufreq_up, cpufreq_up);
store_one(cpufreq_down, cpufreq_down);
store_one(cycle_up, cycle_up);
store_one(cycle_down, cycle_down);
store_one(cpu_boost, cpu_boost);
store_one(boost_freq, boost_freq);

static ssize_t enabled_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", enabled);
}

static ssize_t enabled_store(struct kobject *kobj,
				      struct kobj_attribute *attr,
				      const char *buf, size_t count)
{
	int ret;
	unsigned int val;

	ret = sscanf(buf, "%u\n", &val);
	if (ret != 1 || val < 0 || val > 1)
		return -EINVAL;
	if (val == enabled)
		return count;
	enabled = val;

	if (enabled)
		asmp_start();
	else
		asmp_stop();
	return count;
}

static struct kobj_attribute enabled_attr =
	__ATTR(enabled, 0644,
		enabled_show,
		enabled_store);

static ssize_t boost_time_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%llu\n", div_u64(boost_time, 1000));
}

static ssize_t boost_time_store(struct kobject *kobj,
				      struct kobj_attribute *attr,
				      const char *buf, size_t count)
{
	int ret;
	u64 val;

	ret = sscanf(buf, "%llu", &val);
	if (ret != 1)
		return -EINVAL;

	boost_time = val * 1000;

	return count;
}

static struct kobj_attribute boost_time_attr =
	__ATTR(boost_time, 0644,
		boost_time_show,
		boost_time_store);

static ssize_t input_interval_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%llu\n", div_u64(input_interval, 1000));
}
static ssize_t input_interval_store(struct kobject *kobj,
				      struct kobj_attribute *attr,
				      const char *buf, size_t count)
{
	int ret;
	u64 val;
	ret = sscanf(buf, "%llu", &val);
	if (ret != 1)
		return -EINVAL;
	input_interval = val * 1000;
	return count;
}
static struct kobj_attribute input_interval_attr =
	__ATTR(input_interval, 0644,
		input_interval_show,
		input_interval_store);

static struct attribute *asmp_attributes[] = {
	&enabled_attr.attr,
	&debug.attr,
	&delay.attr,
	&hotplug_suspend.attr,
	&max_cpus.attr,
	&min_cpus.attr,
	&minmax_cpus_scroff.attr,
	&cpufreq_up.attr,
	&cpufreq_down.attr,
	&cycle_up.attr,
	&cycle_down.attr,
	&boost_time_attr.attr,
	&input_interval_attr.attr,
	&cpu_boost.attr,
	&boost_freq.attr,
	NULL
};

static struct attribute_group asmp_attr_group = {
	.attrs = asmp_attributes,
};

/****************************** SYSFS END ******************************/

static int __init asmp_init(void)
{
	unsigned int cpu;
	int rc;

	asmp_kobject = kobject_create_and_add("autosmp", kernel_kobj);
	if (asmp_kobject) {
		rc = sysfs_create_group(asmp_kobject, &asmp_attr_group);
		if (rc)
			pr_warn(ASMP_TAG"ERROR, create sysfs group");
	} else {
		pr_warn(ASMP_TAG"ERROR, create sysfs kobj");
	}
		pr_info(ASMP_TAG"initialized\n");
	if (enabled)
		asmp_start();
	return 0;
}
late_initcall(asmp_init);
