# !!! WHEN THIS FILE IS UPDATED WITH A NEW KERNEL VERSION !!!
# A COPY OF PREVIOUS DeYuS.conf VERSION IS MADE IN /data
#
# AND IF YOU WANT IT TO BE ALWAYS DISABLED
# CREATE A "DeYuSdisabler" file in /data(just the name of file matter)

### CPU/GPU ###
#Apply whole cpu/gpu settings (0/1)
CPUSET=1

#BCL enable yes/no (Disabling it may cause device shutdown)
BCLE=yes

#Enable whole thermal work ot not (0/1)
THERMAL=1

#Msm Limiter (0/1)
MSMLIMITER=1

#Max CPU frequency (Available list in /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies)
MAX=2649600
#Min CPU frequency
MIN=35800
#Suspend CPU frequency
SUSP=1728000

#Governor CPU (Available list in /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors)
GOV=relaxed
GOV2=chill
GOV3=chill
GOV4=chill

#HOTPLUG (autosmp/alucard/blulug/bricked/mako/thunderplug/intelliplug)
HOTPLUG=autosmp

#Workqueue power eficient (Y/N)
PWREFF=Y

#CPU sched policy (powersaving/performance)
SCHEDPOLICY=powersaving

#Limit frequency with thermal settings (Y/N) and (1/0) for unplug core
FREQLIMIT=Y
CORETROL=1

#Temperature in °C to start thermal limitation and temp to unplug core
TEMP=65
TEMPO=85

#Time between check for thermal work
POLLMS=250

#Cpu boost (Y/N)
CPUBOOST=Y
#Input Boost (0/1)
INBOOST=1
#Wakeup boost (0/1)
WKBOOST=1
#Touch boost frequency
TB1=806400
TB2=806400
TB3=806400
TB4=806400
#Sync frequency when boosting cpu
SYNCTHRES=1267200

#GPU governor
GPU=msm-adreno-tz
#GPU max frequency
GPUFREQ=600000000
#Adreno idler (Y/N)
ADRIDLER=Y
#Simple gpu algorithm (0/1)
SIMPLEG=1

### SCREEN CALIBRATION ###
#Tap 2 wake (0-1-2-3)
T2W=0
#Double tap 2 wake (0-1-2-3)
DT2W=1
#Double tap 2 sleep (0/1)
DT2S=0
#Sweep 2 wake (0-15)
S2W=15
#Sweep 2 sleep (0-1-2-3)
S2S=0
#Camera gesture (0/1)
CAM=0
#Timer enabled (0/1)
TIMERE=1
#Time between wakeup TWT for tap and SWT for sweep
TWT=2000
SWT=10000

#Apply whole screen settings (0/1)
KCALSET=1
#KCAL control (0/1)
KCALE=1
#Minimum value
KALMIN=200
#Saturation intensity
KALSAT=245
#Screen value
KALVAL=258
#Screen contrast
KALCONT=268
#Color hue
KALHUE=1515
#Backlight dimmer (Y/N)
BKLDIM=Y

### KERNEL FEATURE ###
#Vibration strengh
VIBS=80

#Enable wakelock (0/1)
WAKELOCK=1

#Enable wakelock when phone is in charge (0/1)
BATTWL=1

#Fastcharge (CAUTION) (0/1)
FSTCHG=0

#Sound locked (0/1)
SPEAKLOCK=1
#High performance mode for low quality audio files (0/1)
SPEAKHIGH=0
#Speaker driver leakage prevention (0/1)
SPEAKLEAK=1

#Apply whole Ram and Z-Ram settings (0/1)
RAMSET=1
#LMK fast run (0/1)
LMKFR=1
#LMK ADAPTIVE make things faster like above (0/1)
LMKAD=1
#LMK cost is something i still don't understand ^^
LMKCT=32
#Min free values for low memory killer to start his job
LMKMF=15360,23040,30720,38400,46080,53760
#Z-Ram (0/1)
ZRAME=1
#Z-Ram compression algorithm
COMPALGO=lz4
#Z-Ram size in byte but can be 780mb for exemple
ZRAMSIZE=782391296

### IO  ###
#Apply whole io settings (0/1)
IOSET=1
#IO scheduler (Available list in /sys/block/mmc*/queue/scheduler)
IOSCHED=maple
#Memory size value
MEMO=1024
MEM=512
NRRO=256
NRR=128

#Fsync (Y/N)
FSYNC=Y
#CRC (0/1)
CRC=0
#KSM enable (0/1) KSMP for pages KSMS for sleeptime
KSME=1
KSMP=256
KSMS=3000
#Global sched/vm tweak (0/1)
SCHVMEAK=1
#Disable some debug feature
DEBUGSET=1

### INTERNET ###
#TCP congestion (Available list in /proc/sys/net/ipv4/tcp_available_congestion_control)
TCP=sociopath
#Goblal internet tweak (0/1)
NETWEAK=1

### SPECIAL ###
#Modify volume step (0/1)
AUDIOSTEP=1
#Volume step number
MEDIASTEP=40
CALLSTEP=20

#Set a custom net hostname (0/1) then choose a name
CHN=0
CHNname=shamu

#Disable system overlay which means gpu composition(Youtube lag fix)
DOVERLAY=0

#Fstrim and drop cache at boot
FSTRIM=1

#Force systemless root yes/no (For SuperSU)
Rlessactive=yes
#Path to systemless
Rless=/data/.supersu

#SeLinux can be permissive-0 or enforcing-1
SELINUX=0

#Build prop tweak (0/1)
BUILDPROPTWEAK=1

#Run script after sdcard mount (0/1)
INITSCRIPT=1
#Folder where script are executed
INITFOLDER=/system/etc/init.d
