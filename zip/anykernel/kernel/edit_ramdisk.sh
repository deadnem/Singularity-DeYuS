#!/sbin/sh
#Ramdisk editing by show-p1984 and modded by Dead-neM

#Unpack ramdisk
mkdir /tmp/ramdisk
cp /tmp/boot.img-ramdisk.gz /tmp/ramdisk/
cd /tmp/ramdisk/
gunzip -c /tmp/ramdisk/boot.img-ramdisk.gz | cpio -i
cd /

#Force Permissive on cmdline
if  grep -qr enforcing= /tmp/boot.img-cmdline; then
 sed -i "s/enforcing=[^ ]*/enforcing=0/" /tmp/boot.img-cmdline
else
 echo $(cat /tmp/boot.img-cmdline) enforcing=0 > /tmp/boot.img-cmdline
fi
if  grep -qr androidboot.selinux= /tmp/boot.img-cmdline; then
 sed -i "s/androidboot.selinux=[^ ]*/androidboot.selinux=permissive/" /tmp/boot.img-cmdline
else
 echo $(cat /tmp/boot.img-cmdline) androidboot.selinux=permissive > /tmp/boot.img-cmdline
fi

#Force vmalloc in cmdline
if  grep -qr vmalloc= /tmp/boot.img-cmdline; then
 sed -i "s/vmalloc=[^ ]*/vmalloc=300M/" /tmp/boot.img-cmdline
else
 echo $(cat /tmp/boot.img-cmdline) vmalloc=300M > /tmp/boot.img-cmdline
fi

#Gain write access on /system
if  grep -qr ro.secure=1 /tmp/ramdisk/default.prop; then
 sed -i "s/ro.secure=1/ro.secure=0/" /tmp/ramdisk/default.prop
fi

#Remove verity
if  grep -qr verity_load_state /tmp/ramdisk/init.shamu.rc; then
 sed -i "s/verity_load_state/#verity_load_state/" /tmp/ramdisk/init.shamu.rc
fi
if  grep -qr verity_update_state /tmp/ramdisk/init.shamu.rc; then
 sed -i "s/verity_update_state/#verity_update_state/" /tmp/ramdisk/init.shamu.rc
fi

#Force SystemLess Root
Rless=/data/.supersu
if grep "SYSTEMLESS=true" $Rless; then
 sleep 0.1
else
 rm $Rless
 echo "SYSTEMLESS=true" >> $Rless
fi

#Modify init file to launch deyus script
INITFILE=/tmp/ramdisk/init.rc
sed -i '/DeYuSs/,/start deyus/d' $INITFILE
echo '# DeYuSs' >> $INITFILE
echo '# edited by Dead-neM from Chainfire Supersu init.supersu.rc file' >> $INITFILE
echo 'service deyus /sbin/DeYuS service' >> $INITFILE
echo '    class late_start' >> $INITFILE
echo '    user root' >> $INITFILE
echo '    seclabel u:r:init:s0' >> $INITFILE
echo '    oneshot' >> $INITFILE
echo 'on boot' >> $INITFILE
echo '    start deyus' >> $INITFILE

#Copy DeYuS script and conf
cp /tmp/DeYuS /tmp/ramdisk/sbin/DeYuS
chmod 777 /tmp/ramdisk/sbin/DeYuS
CONFVERSION=$(grep "" /tmp/DeYuS.conf)
CONFVERSIONold=$(grep "" /data/DeYuS.conf)
#Copy DeYuS script and conf
if [ "$CONFVERSION" = "$CONFVERSIONold" ]; then
 sleep 0.1
else
 mv /data/DeYuS.conf /data/DeYuS.conf.backup
 cp /tmp/DeYuS.conf /data/DeYuS.conf
fi
chmod 777 /data/DeYuS.conf

#Copy fstab
cp /tmp/fstab.shamu /tmp/ramdisk/fstab.shamu
chmod 750 /tmp/ramdisk/fstab.shamu

#Copy custom init.shamu.power.rc
cp /tmp/init.shamu.power.rc /tmp/ramdisk/init.shamu.power.rc
chmod 750 /tmp/ramdisk/init.shamu.power.rc

#Repack ramdisk
rm /tmp/ramdisk/boot.img-ramdisk.gz
rm /tmp/boot.img-ramdisk.gz
cd /tmp/ramdisk/
find . | cpio -o -H newc | gzip > ../boot.img-ramdisk.gz
cd /
rm -rf /tmp/ramdisk

