/* SOCIOPATH TCP
 * Make SocioPath work like his name implies
 * by DeadneM - deadnem@gmail.com
 * 2017
 *
 * Sally Floyd's High Speed TCP (RFC 3649) congestion control
 *
 * See http://www.icir.org/floyd/hstcp.html
 *
 * John Heffner <jheffner@psc.edu>
 */

#include <linux/mm.h>
#include <linux/module.h>
#include <linux/skbuff.h>
#include <linux/inet_diag.h>
#include <net/tcp.h>
#ifdef CONFIG_POWERSUSPEND
#include <linux/powersuspend.h>
#endif

static int soul = 1;
module_param(soul, int, 0644);
MODULE_PARM_DESC(soul, "Only if you have one");
static int ego = 20;
module_param(ego, int, 0644);
MODULE_PARM_DESC(ego, "Degree of sociopathy");
unsigned int sociopath_suspend = 0;

/* From AIMD tables from RFC 3649 appendix B,
 * with fixed-point MD scaled <<8.
 */
static const struct sociopath_aimd_val {
	unsigned int cwnd;
	unsigned int md;
} sociopath_aimd_vals[] = {
	{     38,  128, /*  0.50 */ },
	{    118,  112, /*  0.44 */ },
	{    221,  104, /*  0.41 */ },
	{    347,   98, /*  0.38 */ },
	{    495,   93, /*  0.37 */ },
	{    663,   89, /*  0.35 */ },
	{    851,   86, /*  0.34 */ },
	{   1058,   83, /*  0.33 */ },
	{   1284,   81, /*  0.32 */ },
	{   1529,   78, /*  0.31 */ },
	{   1793,   76, /*  0.30 */ },
	{   2076,   74, /*  0.29 */ },
	{   2378,   72, /*  0.28 */ },
	{   2699,   71, /*  0.28 */ },
	{   3039,   69, /*  0.27 */ },
	{   3399,   68, /*  0.27 */ },
	{   3778,   66, /*  0.26 */ },
	{   4177,   65, /*  0.26 */ },
	{   4596,   64, /*  0.25 */ },
	{   5036,   62, /*  0.25 */ },
	{   5497,   61, /*  0.24 */ },
	{   5979,   60, /*  0.24 */ },
	{   6483,   59, /*  0.23 */ },
	{   7009,   58, /*  0.23 */ },
	{   7558,   57, /*  0.22 */ },
	{   8130,   56, /*  0.22 */ },
	{   8726,   55, /*  0.22 */ },
	{   9346,   54, /*  0.21 */ },
	{   9991,   53, /*  0.21 */ },
	{  10661,   52, /*  0.21 */ },
	{  11358,   52, /*  0.20 */ },
	{  12082,   51, /*  0.20 */ },
	{  12834,   50, /*  0.20 */ },
	{  13614,   49, /*  0.19 */ },
	{  14424,   48, /*  0.19 */ },
	{  15265,   48, /*  0.19 */ },
	{  16137,   47, /*  0.19 */ },
	{  17042,   46, /*  0.18 */ },
	{  17981,   45, /*  0.18 */ },
	{  18955,   45, /*  0.18 */ },
	{  19965,   44, /*  0.17 */ },
	{  21013,   43, /*  0.17 */ },
	{  22101,   43, /*  0.17 */ },
	{  23230,   42, /*  0.17 */ },
	{  24402,   41, /*  0.16 */ },
	{  25618,   41, /*  0.16 */ },
	{  26881,   40, /*  0.16 */ },
	{  28193,   39, /*  0.16 */ },
	{  29557,   39, /*  0.15 */ },
	{  30975,   38, /*  0.15 */ },
	{  32450,   38, /*  0.15 */ },
	{  33986,   37, /*  0.15 */ },
	{  35586,   36, /*  0.14 */ },
	{  37253,   36, /*  0.14 */ },
	{  38992,   35, /*  0.14 */ },
	{  40808,   35, /*  0.14 */ },
	{  42707,   34, /*  0.13 */ },
	{  44694,   33, /*  0.13 */ },
	{  46776,   33, /*  0.13 */ },
	{  48961,   32, /*  0.13 */ },
	{  51258,   32, /*  0.13 */ },
	{  53677,   31, /*  0.12 */ },
	{  56230,   30, /*  0.12 */ },
	{  58932,   30, /*  0.12 */ },
	{  61799,   29, /*  0.12 */ },
	{  64851,   28, /*  0.11 */ },
	{  68113,   28, /*  0.11 */ },
	{  71617,   27, /*  0.11 */ },
	{  75401,   26, /*  0.10 */ },
	{  79517,   26, /*  0.10 */ },
	{  84035,   25, /*  0.10 */ },
	{  89053,   24, /*  0.10 */ },
};

#define SOCIOPATH_AIMD_MAX	ARRAY_SIZE(sociopath_aimd_vals)

struct sociopath {
	u32	ai;
};

static void sociopath_init(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct sociopath *ca = inet_csk_ca(sk);

	ca->ai = 0;
	/* Ensure the MD arithmetic works.  This is somewhat pedantic,
	 * since I don't think we will see a cwnd this large. :) */
	tp->snd_cwnd_clamp = min_t(u32, tp->snd_cwnd_clamp, 0xffffffff/128);
}

static void sociopath_cong_avoid(struct sock *sk, u32 ack, u32 in_flight)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct sociopath *ca = inet_csk_ca(sk);

	if (!tcp_is_cwnd_limited(sk, in_flight))
		return;

	if ((soul) || (!sociopath_suspend)) {
		if (tp->snd_cwnd <= tp->snd_ssthresh)
			tcp_slow_start(tp);
		else {
			/* Update AIMD parameters.
			 *
			 * We want to guarantee that:
			 *     sociopath_aimd_vals[ca->ai-1].cwnd <
			 *     snd_cwnd <=
			 *     sociopath_aimd_vals[ca->ai].cwnd
			 */
			if (tp->snd_cwnd > sociopath_aimd_vals[ca->ai].cwnd) {
				while (tp->snd_cwnd > sociopath_aimd_vals[ca->ai].cwnd &&
				       ca->ai < SOCIOPATH_AIMD_MAX - 1)
					ca->ai++;
			} else if (ca->ai && tp->snd_cwnd <= sociopath_aimd_vals[ca->ai-1].cwnd) {
				while (ca->ai && tp->snd_cwnd <= sociopath_aimd_vals[ca->ai-1].cwnd)
					ca->ai--;
			}
			/* Do additive increase */
			if (tp->snd_cwnd < tp->snd_cwnd_clamp) {
				/* cwnd = cwnd + a(w) / cwnd */
				tp->snd_cwnd_cnt += ca->ai + 1;
				if (tp->snd_cwnd_cnt >= tp->snd_cwnd) {
					tp->snd_cwnd_cnt -= tp->snd_cwnd;
					tp->snd_cwnd++;
				}
			}
		}
	} else {
		if (tp->snd_cwnd <= tp->snd_ssthresh) {
			if (tp->snd_cwnd < ego*10) tp->snd_cwnd = ego*10;
				tcp_reno_cong_avoid(sk, ack, in_flight);
		} else {
		tcp_reno_cong_avoid(sk, ack, in_flight);
		}
	}
}

static u32 sociopath_ssthresh(struct sock *sk)
{
	const struct tcp_sock *tp = tcp_sk(sk);
	const struct sociopath *ca = inet_csk_ca(sk);
	if ((soul) || (!sociopath_suspend)) {
		/* Do multiplicative decrease */
		return max(tp->snd_cwnd - ((tp->snd_cwnd * sociopath_aimd_vals[ca->ai].md) >> 8), 2U);
	} else {
		u32 cwnd = tcp_sk(sk)->snd_cwnd;
		return max(cwnd-(cwnd/ego), 2U);
	}
}

static struct tcp_congestion_ops tcp_sociopath __read_mostly = {
  .init		= sociopath_init,
  .ssthresh	= sociopath_ssthresh,
  .cong_avoid	= sociopath_cong_avoid,

  .owner	= THIS_MODULE,
  .name		= "sociopath"
};


#ifdef CONFIG_POWERSUSPEND
static void sociopath_early_suspend(struct power_suspend *handler) {
sociopath_suspend = 1;
}

static void sociopath_late_resume(struct power_suspend *handler) {
sociopath_suspend = 0;
}

static struct power_suspend sociopath_power_suspend_handler = {
	.suspend = sociopath_early_suspend,
	.resume = sociopath_late_resume,
};
#endif


static int __init tcp_sociopath_register(void) {
  tcp_register_congestion_control(&tcp_sociopath);
#ifdef CONFIG_POWERSUSPEND
  register_power_suspend(&sociopath_power_suspend_handler);
#endif
  return 0;
}

static void __exit tcp_sociopath_unregister(void) {
  tcp_unregister_congestion_control(&tcp_sociopath);
#ifdef CONFIG_POWERSUSPEND
  unregister_power_suspend(&sociopath_power_suspend_handler);
#endif
}

module_init(tcp_sociopath_register);
module_exit(tcp_sociopath_unregister);

MODULE_AUTHOR("Michael Lucy & Sally Floyd & John Heffner & DeadneM");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("TCP Sociopath");
